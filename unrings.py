import os
import re
import numpy as np
import math
import matplotlib.pyplot as plt
import pandas as pd
from analytical import *

# num = 0 # 0 20 MHz; 1 40 MHz;
# len5g = 64
# elen5g = 38

num = 1 # 0 20 MHz; 1 40 MHz;
len5g = 45
elen5g = 27

# num = 2 # 0 20 MHz; 1 40 MHz;
# len5g = 30
# elen5g = 21

# num = 3 # 0 20 MHz; 1 40 MHz;
# len5g = 21
# elen5g = 14

mcs = 5
tptBianchi5g = []
tptBianchi6g = []
distance5g = []
distance6g = []

txPowerDbm5g = 30
txPowerDbm5gSta = 24
channelWidth = 20*(math.pow(2,num))
txPowerDbm6g = 10*math.log10(3.16  * channelWidth)
txPowerDbm6gSta = 10*math.log10(0.8  * channelWidth)


data_rates = [
    [8.603e6, 17.206e6, 25.8e6, 34.4e6, 51.6e6, 68.8e6, 77.4e6, 86e6, 103.2e6, 114.7e6, 129e6, 143.4e6], 
    [17.2e6, 34.4e6, 51.6e6, 68.8e6, 103.2e6, 137.6e6, 154.9e6, 172.1e6, 206.5e6, 229.4e6, 258.1e6, 286.8e6],
    [36e6, 72.1e6, 108.1e6, 144.1e6, 216.2e6, 288.2e6, 324.3e6, 360.3e6, 432.4e6, 480.4e6, 540.4e6, 600.5e6],
    [72.1e6, 144.1e6, 216.2e6, 288.2e6, 432.4e6, 576.5e6, 648.5e6, 720.6e6, 864.7e6, 960.8e6, 1080.9e6, 1201e6]
]

ack_rates = [
    [6e6, 12e6, 12e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6],
    [6e6, 12e6, 12e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6],
    [6e6, 12e6, 12e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6],
    [6e6, 12e6, 12e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6, 24e6],
]
colors = ["r", "g", "b", "m"]
markers = ["*", "x", "o", "^"] 
linestyles = ["-", "--", "-.", ":"]

paths = ["ring_n10_00", "ring_n10_10", "ring_n10_30", "ring_n10_99"]
distance = []
throughput = []
for path in paths:
    files= os.listdir(path)
    dis = []
    for flie in files: 
        if not os.path.isdir(flie): 
            dir5g = "mpdu1_band" + str(int(channelWidth)) + "_mcs5_fre5_dis"
            if dir5g in flie:
                nums = re.findall("\d+", flie)
                # print(nums)
                dis.append(int(nums[4]))
    dis.sort()
    distance.append(dis)
    tpt = []
    for i in range(len(dis)):
        data_dir_5 =  path + "/mpdu1_band" + str(int(channelWidth)) + "_mcs5_fre5_dis"+ str(int(dis[i]))
        tpt_5 = np.array(data_analysis(data_dir_5))
        tpt.append(tpt_5[0])    
    throughput.append(tpt)
plt.figure(1)
for i in range(len(paths)):
    string = paths[i]
    x = string.split("_", 2)
    print(x)
    labels = "d2 = " + str(float(1- int(x[2])/100)) + "d1"
    print(labels)
    plt.plot(distance[i],throughput[i], label = labels, color = colors[i], marker = markers[i], linestyle = linestyles[i])

filename = 'mcs' + str(int(mcs)) +'_unequal_ring' + str(int(channelWidth)) + '.png'
plt.xlabel('Distance d1 (m)')
plt.ylabel('Aggregated Throughput (Mbps)')
plt.legend()
plt.savefig(filename,dpi=300,format='png')
plt.show()
